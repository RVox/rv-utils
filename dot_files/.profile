## Author: RorraVox
## File name: .profile

## INSERT IN "~/.<shell>rc" FILE
# [[ -f ~/.profile ]] && . ~/.profile

TERM_GREEN_FG='\[\e[32m\]'
TERM_ORANGE_FG='\[\e[33m\]'
TERM_RESET='\[\e[0m\]'

# PRE CURSOR TEXT
#PS1='[</°¡°\> \u@\h:\w]\$ '
PS1="[</°¡°\> ${TERM_GREEN_FG}\u${TERM_RESET}@${TERM_ORANGE_FG}\h${TERM_RESET}:\w]\$ "
# \u user
# \h host
# \$ UID = 0 => un else $
# \w current working directory, with $HOME abbreviated with a tilde

# check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize

[[ -f ~/.shell_alias ]] && . ~/.shell_alias # ALIAS

### HISTORY (exported)
HISTTIMEFORMAT="%Y/%m/%d %T " # HISTORY FILE FORMAT
HISTSIZE=10000 # SHELL HISTORY ENTRIES
HISTFILESIZE=200000 # HISTORY FILE ENTRIES
PROMPT_COMMAND="history -a; $PROMPT_COMMAND" # APPEND LAST COMMAND IMMEDIATLY TO HISTORY

### PREFERRED PROGRAMS
SHELL=bash

PAGER=less
EDITOR=nano
VISUAL=gedit
BROWSER=firefox

# $-	current shell options
# *i*	i = interactive option
[[ $- = *i* ]] && neofetch --ascii_distro arch
