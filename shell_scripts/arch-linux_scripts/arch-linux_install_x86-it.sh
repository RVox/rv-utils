#!/bin/bash

## Author: RorraVox
## File name: arch-linux_install_x86-any_it.sh
## LAST TESTED ARCHLINUX VERSION: archlinux-2016.08.01-dual

#[START SETTINGS SECTION]
device=none
# disk_label= #no more 16 char
connect_script="$(dirname $0)/arch-linux_connect.sh"
arch_chroot_script=arch-chroot_script.sh
#[END SETTINGS SECTION]

#GET CPU BIT 32/64
if [ "$(grep -o -w "lm" /proc/cpuinfo)" != "" ]; then
	op_mode_bit=64
else
	op_mode_bit=32
fi

#DISKLABEL CHECK
if	[ -z "$disk_label" ]; then
	disk_label="archlinux_x86-$op_mode_bit"
fi

#CONNECTION SCRIPT CHECK
if [ -e "$connect_script" ]; then
	if ! [ -x "$connect_script" ]; then
		echo -e "\e[31mERROR\e[0m: connect_script: $connect_script file have not execute permission!" >&2
		exit 1
	fi
else
	echo -e "\e[31mERROR\e[0m: connect_script: $connect_script file does not exist!" >&2
	exit 1
fi

#KEYBOARD LAYOUT
loadkeys it

#CONNECTION SCRIPT
source $connect_script

#DEVICE SELECTION
echo "=>Device selection"
while ! [ -b "/dev/$device" ]; do
	ls -1 /dev/sd? | xargs -n 1 basename
	read -p "Select device to install: " -r -n 3 device
	echo
	if ! [ -b "/dev/$device" ]; then
		echo -e "\e[31mERROR\e[0m: /dev/$device does not exist!" >&2
	fi
done

#FIRMWARE SELECTION
echo "=>Firmware selection"
while true; do
	read -p "Choose firmware 1)BIOS or 2)UEFI [1/2] " -r -n 1 firmware
	echo
	case "$firmware" in
		[12]*)
			if [ $firmware -eq 1 ]; then
				firmware="BIOS"
			elif [ $firmware -eq 2 ]; then
				firmware="UEFI"
			fi
			break
		;;
		*) echo "Please type [1] for BIOS or [2] for UEFI";;
	esac
done

#PRINTIG CONFIGURATION
echo
echo "CONFIGURATION SELECTED"
echo "==>device selected: $device"
echo "==>disk_label: $disk_label"
echo "==>op_mode_bit: $op_mode_bit"
echo "==>firmware: $firmware"
echo

#INSTALLATION CONFIRM
echo "=>INSTALLING ARCH-LINUX x86-$op_mode_bit $firmware"
while true; do
	read -p "Start the installation? [y/n] " -r -n 1 yn
	echo
	case "$yn" in
		[Yy]*) break;;
		[Nn]*) exit 0;;
		*) echo "Please type [Yy] or [Nn]";;
	esac
done

#PARTITIONING AND FORMATTING
echo "=>Partitioning and Formatting"
if [ "$firmware" == "BIOS" ]; then
	parted /dev/$device mktable msdos
	parted /dev/$device mkpart primary linux-swap 1MiB 501MiB
	parted /dev/$device mkpart primary ext4 501MiB 100%
	parted /dev/$device set 2 boot on

	mkfs.ext4 -j -L $disk_label /dev/$device\2
	mkdir --verbose /mnt/$disk_label
	mount /dev/$device\2 /mnt/$disk_label
	mkdir --verbose /mnt/$disk_label/boot
	#SWAP
	mkswap /dev/$device\1
	swapon /dev/$device\1
elif [ "$firmware" == "UEFI" ]; then
	parted /dev/$device mktable gpt
	parted /dev/$device mkpart " " fat32 1MiB 501MiB
	parted /dev/$device mkpart " " ext4 501MiB 100%
	parted /dev/$device set 1 esp on

	mkfs.vfat -F 32 /dev/$device\1
	mkfs.ext4 -j -L $disk_label /dev/$device\2
	mkdir --verbose /mnt/$disk_label
	mount /dev/$device\2 /mnt/$disk_label
	mkdir --verbose /mnt/$disk_label/boot
	mount /dev/$device\1 /mnt/$disk_label/boot
else
	exit 1
fi

#PACMAN
echo "=>Pacman ranking mirrors"
rm /etc/pacman.d/mirrorlist
wget -O /etc/pacman.d/mirrorlist.all https://www.archlinux.org/mirrorlist/all/
sed '/^#\S/ s|#||' -i /etc/pacman.d/mirrorlist.all
rankmirrors -n 10 -v /etc/pacman.d/mirrorlist.all | tee /etc/pacman.d/mirrorlist
#### reflector --ipv4 --age 1000 --fastest 10 --n 10 --save /etc/pacman.d/mirrorlist
#### reflector --verbose --latest 5 --sort rate --protocol https --age 12 --save /etc/pacman.d/mirrorlist
#### reflector --verbose --latest 5 --sort rate --protocol https --age 12 --info

#PACSTRAP
echo "=>System installation (pacstrap)"
pacstrap /mnt/$disk_label base linux linux-firmware

#FSTAB (arch-install-scripts)
echo "=>FSTAB"
genfstab -U -p /mnt/$disk_label | tee /mnt/$disk_label/etc/fstab

#CHROOT SCRIPT
echo "=>arch-chroot script"
cat > /mnt/$disk_label/$arch_chroot_script << EOF
##BEGIN CHROOT FILE
zoneinfo_path=/Europe/Rome
locale_gen=it_IT.UTF-8\ UTF-8
lang=it_IT.UTF-8
local_time=it_IT.UTF-8
keymap=it

echo
echo "==>zoneinfo_path: \$zoneinfo_path"
echo "==>locale_gen: \$locale_gen"
echo "==>lang: \$lang"
echo "==>local_time: \$local_time"
echo "==>keymap: \$keymap"
echo

##BEGIN CHROOT SECTION
####ln -s /usr/share/zoneinfo/Europe/Rome /etc/localtime
ln -s /usr/share/zoneinfo\$zoneinfo_path /etc/localtime
####echo -e "\n#SELECTED\nit_IT.UTF-8 UTF-8" >> /etc/locale.gen
echo -e "\n#SELECTED\n\$locale_gen" >> /etc/locale.gen
locale-gen
####echo -e "LANG=it_IT.UTF-8\nLC_COLLATE=\"C\"\nLC_TIME=\"it_IT.UTF-8\"" >> /etc/locale.conf
echo -e "LANG=\$lang\nLC_COLLATE=\"C\"\nLC_TIME=\"\$local_time\"" >> /etc/locale.conf
####echo -e "KEYMAP=it" >> /etc/vconsole.conf
echo -e "KEYMAP=\$keymap" >> /etc/vconsole.conf
mkinitcpio -p linux
echo "Enter root user password for new installation..."
passwd
#pacman --noconfirm -S linux linux-firmware
pacman --noconfirm -S intel-ucode amd-ucode
pacman --noconfirm -S dhcpcd iw iwd
pacman --noconfirm -S sudo nano htop man
pacman --noconfirm -S grub
pacman --noconfirm -S efibootmgr efivar
#pacman --noconfirm -S os-prober	#for windows dual boot
EOF
if [ "$firmware" == "UEFI" ]; then
	echo "grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=archlinux_grub --recheck" >> /mnt/$disk_label/$arch_chroot_script
else
	echo "grub-install --target=i386-pc --boot-directory=/boot --recheck /dev/$device" >> /mnt/$disk_label/$arch_chroot_script
fi
cat >> /mnt/$disk_label/$arch_chroot_script << EOF
#os-prober
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable dhcpcd
timedatectl set-ntp true

exit 0
##END CHROOT SECTION
##END CHROOT FILE
EOF
chmod 777 /mnt/$disk_label/$arch_chroot_script
arch-chroot /mnt/$disk_label ./$arch_chroot_script

#CLEANING
echo "=>Cleaning..."
rm /mnt/$disk_label/$arch_chroot_script
umount -R /mnt/$disk_label
rmdir /mnt/$disk_label/
echo "=>Cleaning completed."

echo "===>SYSTEM INSTALLATION COMPLETED<==="
#REBOOT
while true; do
	read -p "Reboot the system? [y/n] " -r -n 1 yn
	echo
	case "$yn" in
		[Yy]*) reboot -f;;
		[Nn]*) exit 0;;
		*) echo "Please type [Yy] or [Nn]";;
	esac
done

exit 0
