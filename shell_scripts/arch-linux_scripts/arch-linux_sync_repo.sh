#!/bin/bash

## Author: Rorra Vox
## File name: arch-linux_sync_repo.sh

## OPTIONS:
# -h --help
# --noconfirm
##TODO
# -c --clear-temp 
# -i --interactive

#C sudo pacman --noconfirm -Syu rsync
#SYNC ARCH-LINUX REPOSITORY UTILITY
#C rsync -rtlvH --delete-after --delay-updates --safe-links --copy-links --progress $repo_mirror_url $repo_mirror_dir

#---------------------------------------------------------------
#[START SETTINGS SECTION]
# repo_mirror_url [ONLY RSYNC]
	repo_mirror_url="rsync://mirror.de.leaseweb.net/archlinux/"
	#repo_mirror_url="rsync://mirror.f4st.host/archlinux/"
	#repo_mirror_url="rsync://mirror.fra10.de.leaseweb.net/archlinux/"
	#repo_mirror_url="rsync://mirror.chaoticum.net/arch/"
	#---
	#repo_mirror_url="rsync://mirrors.tuna.tsinghua.edu.cn/archlinuxarm/"
	#repo_mirror_url="rsync://dk.mirror.archlinuxarm.org/archlinuxarm/"

# last_update_url [ONLY HTTP/HTTPS] (ATTENTION: use same url of repo_mirror_url)
	#last_update_url="https://mirror.de.leaseweb.net/archlinux/lastupdate"
	last_update_url="https://mirror.fra10.de.leaseweb.net/archlinux/lastupdate"

# repo_mirror_dir
	repo_mirror_dir="$(dirname $0)/mirror.archlinux/"
	#repo_mirror_dir="$(dirname $0)/mirror.archlinuxarm/"

# repo_list
	repo_list=(core community extra iso multilib pool)
	#repo_list=(aarch64 armv7h arm)
#[END SETTINGS SECTION]
#---------------------------------------------------------------

lock_file="${repo_mirror_dir}arch-linux_sync_repo.lock"

rsync_opts="--recursive --times --links --hard-links --delete-after --delay-updates --safe-links --progress --human-readable --verbose"
#rsync_opts=--recursive --perms --links --size-only --delete-after --itemize-changes --progress --verbose"

# -r	--recursive			recurse into directories
# -t	--times				preserve modification times
# -l	--links				copy symlinks as symlinks
# -H	--hard-links		preserve hard links
# -v	--verbose			increase verbosity
# --	--delay-updates		put all updated files into place at transfer's end
# --	--progress			show progress during transfer
# -h	--human-readable	output numbers in a human-readable format
# --	--safe-links		ignore symlinks that point outside the source tree

# -L	--copy-links		transform symlink into referent file/dir

# -p	--perms				preserve permissions
# --	--size-only			skip files that match in size
# --	--delete-after		receiver deletes after transfer, not during
# -i	--itemize-changes	output a change-summary for all updates
# -n	--dry-run			perform a trial run with no changes made

rsync_repo_list="lastsync,lastupdate"

remote_last_update="$(curl -Ls ${last_update_url} 2>/dev/null)"
local_last_update="$(cat ${repo_mirror_dir}/lastupdate 2>/dev/null)"

if ! [ -z "${repo_list}" ]; then
	for repo in "${repo_list[@]}" ; do
		rsync_repo_list+=",${repo}"
	done
else
	repo_list="*"
fi

#---------------------------------------------------------------
rsyncExec() {
	local cmd_rsync_exec="rsync ${rsync_opts} ${repo_mirror_url}{${rsync_repo_list}} ${repo_mirror_dir}" #sync arch-linux repository
	eval "${cmd_rsync_exec}"
}
rsyncPrintSettings() {
	local print
	print+="=> \e[32mSYNC ARCH-LINUX REPOSITORY UTILITY\e[0m\n"
	print+="==> FROM MIRROR URL: ${repo_mirror_url}\n"
	print+="==> TO DIR: ${repo_mirror_dir}\n"
	print+="==> REPOS TO SYNC: \e[33m${repo_list[@]}\e[0m\n"
	print+="==> RSYNC OPTIONS: ${rsync_opts}\n"
	print+="==> LASTUPDATE: LOCAL_\e[33m${local_last_update}\e[0m($(date -d @${local_last_update} 2>/dev/null)) | REMOTE_\e[33m${remote_last_update}\e[0m($(date -d @${remote_last_update}2>/dev/null))"
	echo -e "${print}"
}
printHelp() {
	local print
	print+="Arch-Linux sync repository utility.\n"
	print+="Use --noconfirm option to sync without confirmation.\n"
	print+="Use --help or -h option to get additional information.\n"
	print+="Default parameters:\n"
	echo -e "${print}"
	rsyncPrintSettings
}
makeChecks() {
	if [ "$(command -v rsync)" == "" ]; then
		echo -e "\e[31mERROR\e[0m: rsync program not found!" >&2
		exit 1
	fi
	if [ -d "${repo_mirror_dir}" ]; then
		if ! [ -w "${repo_mirror_dir}" ]; then
			echo -e "\e[31mERROR\e[0m: no write permission ($repo_mirror_dir)!" >&2
			exit 1
		fi
	else
		if mkdir -m=744 -p "${repo_mirror_dir}" ; then
			echo "=> Created dir ${repo_mirror_dir}"
		else
			echo -e "\e[31mERROR\e[0m: can't create dir (${repo_mirror_dir})!" >&2
			exit 1
		fi
	fi
}
#function errorExit {}

#CLEAR RSYNC .~tmp~ FILES
clearRsyncTempFiles() {
	rsync_temp_files=$(find $repo_mirror_dir -name .~tmp~)

	if [ "$1" != "--noconfirm" ]; then
		if [ "$rsync_temp_files" != "" ]; then
			echo -e "Found:\n$rsync_temp_files"
			while true; do
				read -p "Delete .~tmp~ files? [y/n] " -r -n 1 yn
				echo
				case "$yn" in
					[Yy]*)
						echo $rsync_temp_files | xargs rm -R
						echo "=> .~tmp~ files deleted."
						break;;
					[Nn]*) break;;
					*) echo "Please type [Yy] or [Nn]";;
				esac
			done
		fi
	fi
}

main() {
	makeChecks
	rsyncPrintSettings

	if [ "$1" != "--noconfirm" ]; then
		while true; do
			read -p "Start syncing? [y/n] " -r -n 1 yn
			echo
			case "$yn" in
				[Yy]*)
					rsyncExec
					break;;
				[Nn]*) break;;
				*) echo "Please type [Yy] or [Nn]";;
			esac
		done
	else
		rsyncExec
	fi
	clearRsyncTempFiles
}
#---------------------------------------------------------------

trap 'exit' 3

# WITHOUT TTY RUN ONLY WHEN THERE ARE CHANGES
if ! tty -s ; then
	if [[ "$local_last_update" != "" ]] && diff -b "$remote_last_update" "$local_last_update" >/dev/null; then
		rsyncExec
	fi
	exit 0
fi

### HELP
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	printHelp
	exit 0
fi

### FILE LOCK
#exec {lock_fd}>$lock_file
#flock -n "$lock_fd" || { echo -e "\e[31mERROR\e[0m: LOCK[$lock_fd|$lock_file] >&2"; exit 1; }

main

exit 0
