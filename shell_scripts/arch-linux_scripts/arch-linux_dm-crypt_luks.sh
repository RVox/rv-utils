#!/bin/bash

## Author: RorraVox
## File name: arch-linux_dm-crypt_luks.sh

#---------------------
# COMMON PROCEDURE

## To create the container:
#C fallocate -l 50GiB secret
#C dd if=/dev/urandom of=/secret bs=1MiB count=51200
#C sudo losetup /dev/loop1 /secret
#C sudo cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 luksFormat /dev/loop1
#C sudo cryptsetup --type luks open /dev/loop1 secret
#C sudo mkfs.ntfs -Q -L diskLabel /dev/mapper/secret

#C sudo mount /dev/mapper/secret /mnt/secret
#C sudo umount /mnt/secret
#C sudo cryptsetup --type luks close secret
#C sudo losetup -d /dev/loop1

## To unmount the container:
#C sudo umount /mnt/secret
#C sudo cryptsetup luksClose secret
#C sudo losetup -d /dev/loop1

## To mount the container again:
#C sudo losetup /dev/loop1 secret
#C	(interactive password) sudo cryptsetup luksOpen /dev/loop1 secret
#C	(shell password) echo -n PA$$WORD | sudo cryptsetup luksOpen /dev/loop1 secret
#C sudo mount /dev/mapper/secret /mnt/secret

## OTHER COMMANDS
#C sudo cryptsetup luksDump /dev/loop1
## LUKS KEYS [0-7]
#C sudo cryptsetup luksAddKey /dev/loop1 (/path/to/<additionalkeyfile>) 
#C sudo cryptsetup luksChangeKey /dev/loop1 -S <key-slot>

#---------------------
# COMMON COMMANDS

#C sudo modprobe loop	USE THIS COMMAND IF NO LOOP DEVICE EXIST
#C sudo udisksctl power-off		POWER OFF EXTERNAL DEVICE
#C sudo udisksctl unmount		UNMOUNT EXTERNAL DEVICE
#C sudo udisksctl mount		MOUNT EXTERNAL DEVICE

#---------------------

getFirstFreeLoopDev() {
	local loop_dev="loop" loop_dev_num=0 loop_dev_num_tot=$(( $(ls /dev/loop* | grep loop | wc -l) - 1 ))

	while [ $loop_dev_num -le $loop_dev_num_tot ]; do
		if [ -e "/dev/${loop_dev}${loop_dev_num}" ]; then
			  if ! [ "$(losetup | grep ${loop_dev}${loop_dev_num})" ]; then
				echo "${loop_dev}${loop_dev_num}"
				return 0
			fi
		fi
		loop_dev_num=$(( loop_dev_num + 1 ))
	done

	echo -e "\e[31mERROR\e[0m: loop device not found!" >&2
	echo "_error"
	return 1
}
getMountedLoopDev() { # $1(container_path)
	local container_path="$1"
	local loop_dev="$(losetup -a | grep -w -e "$container_path" | cut -d: -f1)"

	if ! [ "$loop_dev" ]; then
		echo -e "\e[31mERROR\e[0m: loop device not found!" >&2
		echo "_error"
		return 1	
	fi

	echo "$loop_dev"
	return 0
}
insertContainerPath() {
	local container_path

	read -e -p "Insert container path: " container_path
	if ! [ -e "$container_path" ]; then
		echo -e "\e[31mERROR\e[0m: container path is not a file ( "$container_path" )!" >&2
		echo "_error"
		return 1
	fi

	container_path="$(realpath "$container_path")"

	echo "$container_path"
	return 0
}
insertContainerFile() {
	local container_file

	read -e -p "Insert container file: " container_file
	if ! [ -d "$(dirname "$container_file")" ]; then
		echo -e "\e[31mERROR\e[0m: container dir is not a dir ( $(dirname "$container_file") )!" >&2
		echo "_error"
		return 1
	else
		if [ -f "$container_file" ]; then
			echo -e "\e[31mERROR\e[0m: container file already exist ( "$container_file" )!" >&2
			echo "_error"
			return 1
		fi
	fi

	container_file="$(dirname "$container_file")/$(basename "$container_file")"
	container_file="$(realpath "$container_file")"

	echo "$container_file"
	return 0
}
insertContainerSizeGiB() {
	local container_size

	read -e -p "Insert container size GiB: " container_size
	if ! [[ "$container_size" =~ ^[0-9]+$ ]] ; then
		echo -e "\e[31mERROR\e[0m: container size is not a GiB size ( $container_size )!" >&2
		echo "_error"
		return 1
	fi

	echo $container_size
	return 0
}
createContainer() {
	local cryptsetup_options="--batch-mode -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 luksFormat"
	local container_file container_path container_temp_name loop_dev
	local container_size_B container_size_KiB container_size_MiB container_size_GiB

	container_file="$(insertContainerFile)"
	if [ "$container_file" == "_error" ]; then
		return 1
	fi
	loop_dev="$(getFirstFreeLoopDev)"
	if [ "$loop_dev" == "_error" ]; then
		return 1
	fi
	container_size_GiB="$(insertContainerSizeGiB)"
	if [ "$container_size" == "_error" ]; then
		return 1
	fi

	container_path="$container_file"
	container_temp_name="$(basename "$container_path")"

	#container_size_GiB=$container_size_GiB
	container_size_MiB=$(( $container_size_GiB * 1024 ))
	container_size_KiB=$(( $container_size_MiB * 1024 ))
	container_size_B=$(( $container_size_KiB * 1024 ))

	#fallocate -l ${container_size}GiB "$container_file" #NOT WORK ON ALL FILESYSTEMS ( NTFS =( )
	echo "==> Set urandom data inside container (size: ${container_size_B}B/${container_size_KiB}KiB/${container_size_MiB}MiB/${container_size_GiB}GiB)"
	dd if=/dev/urandom of="$container_path" bs=1MiB count=$container_size_MiB status=progress

	sudo losetup /dev/$loop_dev "$container_path"
	sudo cryptsetup ${cryptsetup_options} /dev/$loop_dev
	sudo cryptsetup luksOpen /dev/$loop_dev $container_temp_name
	sudo mkfs.btrfs -L "$container_temp_name" /dev/mapper/$container_temp_name

	sudo cryptsetup luksClose $container_temp_name
	sudo losetup -d /dev/$loop_dev

	echo -e "=> Container succesfully created ( file://"$container_path" )"
	return 0
}
openContainer_interactive() {
	local container_path mount_name loop_dev

	container_path="$(insertContainerPath)"
	if [ "$container_path" == "_error" ]; then
		return 1
	fi
	loop_dev="$(getFirstFreeLoopDev)"
	if [ "$loop_dev" == "_error" ]; then
		return 1
	fi

	mount_name="$(basename "$container_path")"
	sudo losetup /dev/$loop_dev "$container_path"
	sudo cryptsetup luksOpen /dev/$loop_dev $mount_name
	sudo mkdir $mount_path/$mount_name
	sudo mount /dev/mapper/$mount_name $mount_path/$mount_name
	sudo chown $USER $mount_path/$mount_name

	echo -e "=> Container succesfully created ( mount:$mount_path/$mount_name )"
	return 0
}
closeContainer() {
	local container_path mount_name loop_dev

	container_path="$(insertContainerPath)"
	if [ "$container_path" == "_error" ]; then
		return 1
	fi
	loop_dev="$(getMountedLoopDev "$container_path")"
	loop_dev="$(basename "$loop_dev")"
	if [ "$loop_dev" == "_error" ]; then
		return 1
	fi

	mount_name="$(basename "$container_path")"
	sudo umount $mount_path/$mount_name
	sudo rmdir $mount_path/$mount_name
	sudo cryptsetup luksClose $mount_name
	sudo losetup -d /dev/$loop_dev

	echo -e "=> Container succesfully closed"
	return 0
}
main() {
	options_values=(
		"1) Create container."
		"2) Open container."
		"3) Close container."
		"q) Quit.")

	mount_path="/run/media/$USER"
	if ! [ -d "$mount_path" ]; then
		mount_path="/mnt"
	fi

	while true; do
		echo ${options_values[@]}
		read -p "Enter option [1-3|q]: " choice
		case $choice in
			1) createContainer;;
			2) openContainer_interactive;;
			3) closeContainer;;
			q) break;;
			*) echo -e "\e[31mERROR\e[0m: option invalid ( $choice )!" >&2 && sleep 2;;
		esac
	done

	return 0
}

main
exit 0

