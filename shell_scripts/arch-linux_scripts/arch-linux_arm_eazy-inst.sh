#!/bin/bash

## Author: RorraVox
## File name: arch-linux_arm_eazy-inst.sh

#[START SETTINGS SECTION]
mass_memory_device="none" #"sdd"

#iso_url="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-3-latest.tar.gz"	#Raspberry Pi 3B/3B+ DEFAULT-ROOTFS.TAR
#iso_url="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-4-latest.tar.gz"	#Raspberry Pi 4B aarch32 DEFAULT-ROOTFS.TAR
iso_url="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz" #Raspberry Pi 4B aarch64 DEFAULT-ROOTFS.TAR

file_path="$(dirname $0)/$(basename ${iso_url})"

boot_filesystem_label="alarm-boot"
root_filesystem_label="alarm-root"
#[END SETTINGS SECTION]

#---------------------------------------------------------------
LOG_Info() {
    local _msg="${1}"
	if [ "${_msg}" != "" ]; then 
		echo -e "\e[34mLOG\e[0m: ${_msg}" >&2
	else 
		echo >&2
	fi
}
LOG_Error() {
	local _code="${1}"
	local _msg="${2}"

    echo -e "\e[31mERROR\e[0m (${_code}): ${_msg}" >&2
	if [ "${_code}" -gt 0 ]; then
        exit "${_code}"
    fi
}
#---------------------------------------------------------------
main() {
	[ "${EUID}" -ne 0 ] && LOG_Error 1 "Run as sudo user."
	! [ -b "/dev/${mass_memory_device}" ] && LOG_Error 1 "Block device not valid (/dev/${mass_memory_device})."

	LOG_Info "SETTINGS:"
	LOG_Info "==> mass_memory_device: ${mass_memory_device}"
	LOG_Info "==> iso_url: ${iso_url}"
	LOG_Info "==> file_path: ${file_path}"
	echo
	
	while true; do
		read -p "Start install process? [y/n] " -r -n 1 yn
		echo
		case "$yn" in
			[Yy]*) break ;;
			[Nn]*) LOG_Error 1 "Aborted." ;;
			*) echo "Please type [Yy] or [Nn]" ;;
		esac
	done

	[ "$(mount | grep /dev/${mass_memory_device})" ] && umount "/dev/${mass_memory_device}"*

	LOG_Info "Partitioning..."
	parted "/dev/${mass_memory_device}" mktable msdos #MBR
	parted "/dev/${mass_memory_device}" mkpart primary fat32 1MiB 501MiB
	parted "/dev/${mass_memory_device}" mkpart primary ext4 501MiB 100%

	LOG_Info "Formatting..."
	mkfs.vfat -F 32 -n "${boot_filesystem_label}" /dev/$mass_memory_device\1
	mkfs.ext4 -j -L "${root_filesystem_label}" /dev/$mass_memory_device\2

	LOG_Info "Mounting..."
	mkdir -p "/mnt/alarm_root"
	mkdir -p "/mnt/alarm_boot"

	mount "/dev/${mass_memory_device}"\1 "/mnt/alarm_boot"
	mount "/dev/${mass_memory_device}"\2 "/mnt/alarm_root"

	LOG_Info "Acquiring..."
	if [ -f "${file_path}" ]; then
		! [ -r "${file_path}" ] && LOG_Error 1 "File not redable (${file_path})."
	else
		wget -P "$(dirname $0)" "${iso_url}"
		{ ! [ -f "${file_path}" ] || ! [ -r "${file_path}" ]; } && LOG_Error 1 "File not downlodable (${iso_url})."
		chmod 777 "${file_path}"
	fi

	LOG_Info "Extracting TAR.GZ..."
	bsdtar -vxpf "${file_path}" -C "/mnt/alarm_root"

	LOG_Info "Installing boot files..."
	mv "/mnt/alarm_root/boot/"* "/mnt/alarm_boot"

	LOG_Info "Enabling services..."
	local service_src_path="/usr/lib/systemd/system"
	local service_dst_path="/mnt/alarm_root/etc/systemd/system/multi-user.target.wants"
	#ln -s -v "${service_src_path}/dhcpcd.service" "${service_dst_path}/dhcpcd.service"
	ln -s -v "${service_src_path}/sshd.service" "${service_dst_path}/sshd.service"
	ln -s -v "${service_src_path}/systemd-networkd.service" "${service_dst_path}/systemd-networkd.service"

	LOG_Info "Syncing..."
	sync "/mnt/alarm_boot"
	sync "/mnt/alarm_root"
	
	LOG_Info "Update /etc/fstab (for the different SD block device compared to the Raspberry Pi 3): sed -i \"s/mmcblk0/mmcblk1/g\" \"/etc/fstab\" "
	sed -i 's/mmcblk0/mmcblk1/g' "/mnt/alarm_root/etc/fstab"

	LOG_Info "Unmounting..."
	umount "/dev/${mass_memory_device}"*

	rmdir "/mnt/alarm_boot"
	rmdir "/mnt/alarm_root"	

	LOG_Info "Complete!"
}
#---------------------------------------------------------------

trap 'exit' 3
main

exit 0

#package(arp-scan)
#sudo arp-scan --interface=enp6s0 192.168.1.1/24

# Use the serial console or SSH to the IP address given to the board by your router.
# alarm		passw: alarm
# root		passw: root

#Initialize the pacman keyring and populate the Arch Linux ARM package signing keys:
#C pacman-key --init
#C pacman-key --populate archlinuxarm

#C timedatectl set-ntp true

# xf86-video-fbdev xorg-xinit dbus gnome gnome-extra mesa libva-mesa-driver mesa-vdpau

#C pacman -Syu

