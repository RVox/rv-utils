#!/bin/bash

## Author: RorraVox
## File name: arch-linux_eazy-iso.sh

### Arch Linux script for burn archlinux-*.*.*-x86_64.iso on a device

## COMMANDS
# (echo, wget, dd, ls, id)

#[START SETTINGS SECTION]
mass_memory_device="sdX"
archlinux_iso_path="$(dirname $0)/"
arch_linux_mirror_url="http://mirror.mikrogravitation.org"
#[END SETTINGS SECTION]

#C wget -c -r -l1 -nd -A ".iso" --accept-regex=".iso" "<arch_linux_mirror_url>/archlinux/iso/latest/"
	#C EX: wget -r -nd -A ".iso" --accept-regex=".iso" "http://mirror.mikrogravitation.org/archlinux/iso/latest/"

#---------------------------------------------------------------
LOG_Info() {
    local _msg="${1}"
	[ "${_msg}" = "" ] && _msg="UNDEFINED."
	echo -e "\e[34mLOG\e[0m: ${_msg}" >&2
}
LOG_Error() {
	local _code="${1}" # IF > 0 -> EXIT
	local _msg="${2}"

	[ "${_code}" = "" ] && _code="1" #NOT EXIT
	[ "${_msg}" = "" ] && _msg="UNDEFINED."

    echo -e "\e[31mERROR\e[0m (${_code}): ${_msg}" >&2
	[ "${_code}" -gt 0 ] && exit "${_code}"
}
#---------------------------------------------------------------
confirm_alert() {
	LOG_Info "=>Burning ARCH-LINUX x86_64 ISO on (/dev/${mass_memory_device})"
	while true; do
		read -p "Start the process? [y/n] " -r -n 1 yn
		echo
		case "$yn" in
			[Yy]*) break ;;
			[Nn]*) LOG_Error 1 "Aborted." ;;
			*) echo "Please type [Yy] or [Nn]" ;;
		esac
	done
}
check_iso_file() {
	LOG_Info "Checking iso file..."

	archlinux_iso_file_path=$(ls ${archlinux_iso_path}archlinux-*.*.*-x86_64.iso)
	LOG_Info "archlinux_iso_file_path: ${archlinux_iso_file_path}"

	if [ -f "${archlinux_iso_file_path}" ] || [ -h "${archlinux_iso_file_path}" ]; then
		! [ -r "${archlinux_iso_file_path}" ] && LOG_Error 1 "File not redable (${archlinux_iso_file_path})."
		LOG_Info "File found (${archlinux_iso_file_path})"
		return 1
	fi
	LOG_Error 0 "File not found (${archlinux_iso_file_path})."
	return 0
}
download_iso_file() {
	local _tmp_dir="/tmp/archlinux_iso"
	local _iso_file_name=""
	local _rc

	LOG_Info "Downloading iso file..."

	local cmd_wget="wget --continue --recursive --level=1 --no-directories --accept='.iso' --accept-regex='.iso' --directory-prefix=\"${_tmp_dir}\" \"$arch_linux_mirror_url/archlinux/iso/latest/\""
	local cmd_wget_exec="${cmd_wget} && _iso_file_name=\$(ls \"${_tmp_dir}\"); mv \"${_tmp_dir}/\${_iso_file_name}\" \"${archlinux_iso_path}\"; rmdir \"${_tmp_dir}\""

	LOG_Info "cmd_wget: ${cmd_wget}"
	eval "${cmd_wget_exec}"
	check_iso_file; _rc="$?"

	if [ "${_rc}" -eq 0 ]; then
		while true; do
			read -p "Try to download again? [y/n] " -r -n 1 yn
			echo
			case "$yn" in
				[Yy]*)
					eval ${cmd_wget_exec}
					check_iso_file; _rc="$?"
					break ;;
				[Nn]*)
					LOG_Error 1 "File not downloaded."
					return 0
					break ;;
				*) echo "Please type [Yy] or [Nn]" ;;
			esac
		done
	fi

	[ -f "${archlinux_iso_file_path}" ] && [ "$(id -nu)" != "$(logname)" ] && chown "$(logname):$(id -ng $(logname))" "${archlinux_iso_file_path}"

	LOG_Info "File succesfully downloaded."
	return 1
}
write_raw_file_on_dev() {
	local _file_path="${1}"
	local _dev_path="${2}"
	local _rc

	LOG_Info "Writing iso file..."

	[ "${_file_path}" = "" ] && { LOG_Error 0 "Empty file path."; return 0; }
	[ "${_dev_path}" = "" ] && { LOG_Error 0 "Empty device path."; return 0; }

	! [ -b "${_dev_path}" ] && LOG_Error 1 "Block device path not valid (${_dev_path})."

	dd bs=4M if="${_file_path}" of="${_dev_path}" status=progress oflag=sync; _rc="$?"

	[ "${_rc}" -eq 0 ] && { LOG_Info "Write complete."; return 1; }
	[ "${_rc}" -gt 0 ] && { LOG_Error 0 "Write failed!"; return 0; }
	LOG_Error 0 "UNDEFINED"
}
main() {
	[ "${EUID}" -ne 0 ] && LOG_Error 1 "Run as sudo user."
	! [ -b "/dev/${mass_memory_device}" ] && LOG_Error 1 "Block device not valid (/dev/${mass_memory_device})."

	confirm_alert

	check_iso_file; [ "$?" -eq 0 ] && download_iso_file; [ "$?" -eq 0 ] && LOG_Error 1 "File not downlodable (${arch_linux_mirror_url})."

	write_raw_file_on_dev "${archlinux_iso_file_path}" "/dev/${mass_memory_device}"
	[ "$?" -eq 1 ] && LOG_Info "Complete!" || LOG_Error 1 "Failed!"
}
#---------------------------------------------------------------

trap 'exit' 3

main
exit 0

