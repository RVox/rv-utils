#!/bin/bash

## Author: RorraVox
## File name: kiss_launcher.sh

# TESTED ONLY WITH BASH

# APPEND IN SWAY CONFIG
#set $menu urxvt -title launcher -e ~/.config/sway/kiss_launcher.sh
#	for_window [title="^launcher" class="URxvt"] floating enable, border none

# NOTES:
# REPLACE FIRST ${parameter/pattern/string}		REPLACE ALL ${parameter//pattern/string}



#[START SETTINGS SECTION]
HIST_FILE="~/.cache/kiss-launcher_history"


# ANSI COLORS DEFINE
ansi_color_reset="\e[39m"
term_prog_ansi_color="\e[32m" #GREEN
gui_prog_ansi_color="\e[33m" #ORANGE
com_hist_ansi_color="\e[34m" #BLUE

#$TERM_EXEC="urxvt -hold -e $SHELL -c "
#[END SETTINGS SECTION]


EXEC_WM_sway() {
	echo -e "$@" > kiss_llog
	if [[ $(grep -E ^$@$ <<< $term_prog_list) ]]; then
		echo "term_prog_list" >> kiss_llog
		#term_prog_exec
	elif [[ $(grep -E ^$@$ <<< $gui_prog_list) ]]; then
		echo "gui_prog_list" >> kiss_llog
		#gui_prog_exec
	else
		echo "error" >> kiss_llog
	fi
	#swaymsg -t command exec "urxvt -hold -e $SHELL -c \"$@\""
	swaymsg -t command exec "$@"
}


# REQUIRE (swaymsg jq)
GET_WM_LAYOUT_sway() {
	# (WORKSPACE)/OUTPUT/[WINDOW, WINDOW...]
	# /OUTPUT/(WORKSPACE)[WINDOW, WINDOW...](WORKSPACE)[WINDOW, WINDOW...]
	# [WINDOW](WORKSPACE)/OUTPUT/
	workspaces=$(swaymsg -t get_workspaces | jq '.[] | [.name, .output, .focused, .representation]')
	# swaymsg -t get_workspaces | jq '.[] | .["name","output","focused","output"]'
	#windows=()
}


#term_prog_exec() {}
#gui_prog_exec() {}
#workspace_switcher() {}
#window_switcher() {}
#workspace_window_switcher() {}
#fs_explorer() {}
#fs_launcher() {}

#command_list=$(compgen -c | sort -u)
##C diff <(echo -e "$(find ${PATH//:/$'\n'} \( -type l -o -type f \) -executable -printf '%f\n' | sort -u)" ) <(echo "$(compgen -c | sort -u)")
term_prog_list=$(find ${PATH//:/$'\n'} \( -type l -o -type f \) -executable -printf '%f\n' | sort --unique)
gui_prog_list=$(find /usr/share/applications/ -maxdepth 1 -type f -name '*.desktop' -printf '%f\n' | sort --unique)


if [ -f "$HIST_FILE" ]; then
    com_hist_list=$(cat "$HIST_FILE")
else
    com_hist_list=""
fi
com_hist_list=$(echo -e "${com_hist_list}" | sed -E 's/^[0-9]+ (.+)$/\1/') # REMOVE HISTORY EXECUTION COUNTER





#ANSI COLORS APPEND TO ALL LIST ELEMS
com_hist_ansi_colored="${com_hist_ansi_color}${com_hist_list[@]//$'\n'/${ansi_color_reset}$'\n'${com_hist_ansi_color}}${ansi_color_reset}"
term_prog_ansi_colored="${term_prog_ansi_color}${term_prog_list[@]//$'\n'/${ansi_color_reset}$'\n'${term_prog_ansi_color}}${ansi_color_reset}"
gui_prog_ansi_colored="${gui_prog_ansi_color}${gui_prog_list[@]//$'\n'/${ansi_color_reset}$'\n'${gui_prog_ansi_color}}${ansi_color_reset}"

# SORT term_prog & gui_prog
command_pool="${term_prog_ansi_colored}\n${gui_prog_ansi_colored}"
command_pool=$(echo -e "${command_pool}" | sort --unique -k1.6) #-k1.6 skip 6 char ansi color code
command_pool="${com_hist_ansi_colored}\n${command_pool}"

command_pool_count=$(echo -e "${command_pool}" | wc --lines)

# search command list
# Extended Search Mode (query) ^... = start with ...
command_exec=$(echo -e "${command_pool}" | \
    fzf --exact --print-query --no-sort --layout=reverse --margin=1,2 --prompt="=>" --ansi --query="^" | \
    tail -n1) \
	|| exit 1

if [ "$command_exec" == "" ]; then
    exit 1
fi






# get full line from history (with count number)
hist_line=$(echo "$com_hist_list" | grep --fixed-strings "$command_exec")


if [ "$hist_line" == "" ]; then
    hist_count=1
else
    # Increment usage count
    hist_count=$(echo "$hist_line" | sed -E 's/^([0-9]+) .+$/\1/')
    ((hist_count++))
    # delete line, to add updated later
    com_hist_list=$(echo "$com_hist_list" | \
		grep --fixed-strings --invert-match "$command_exec")
fi

# update history
update_line="${hist_count} ${command_exec}"
echo -e "${update_line}\n${com_hist_list}" | \
    sort --numeric-sort --reverse > "$HIST_FILE"




EXEC_WM_sway ${command_exec}

