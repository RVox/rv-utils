#!/bin/bash

## Author: RorraVox
## File name: new_sudo_user.sh

if [ "$#" -eq 0 ]; then
	username="user"
else
	username="${1}"
fi

while true; do
	if [ "$(cat /etc/passwd | grep -w "${username}:")" = "" ]; then
		read -p "Create user: ${username} [y/n] " -r -n 1 yn
		echo -e
		case "$yn" in
			[Yy]*)
				useradd -m ${username}
				passwd ${username}
				echo -e "${username} ALL=(ALL) ALL" >> "/etc/sudoers.d/${username}"
				break
			;;
			[Nn]*)
				read -p "Enter username: " -r username
				continue
			;;
			*) echo "Please type [Yy] or [Nn]";;
		esac
	else
		echo -e "\e[31mERROR\e[0m: username ${username} already exist!"
		read -p "Enter username: " -r username
	fi
done

exit 0
