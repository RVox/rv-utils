#!/bin/bash

## Author: RorraVox
## File name: git_cmd_all.sh

#C	find . -type d -print0 | xargs -0 chmod 755
#C	find . -type f -print0 | xargs -0 chmod 644

#[START SETTINGS SECTION]
base_dir="$(realpath $(dirname ${0})/)"
#GIT CMD
### WRITE
	#git_cmd='git -C "${git_dir}" reset --hard'
	git_cmd='echo -e "==> \\e[32m$(getGitRefURL ${git_dir} origin)\\e[0m"; git -C "${git_dir}" pull origin --rebase'

	#git_cmd='setGitProto "${git_dir}" "origin" "git"'
	#git_cmd='setGitProto "${git_dir}" "origin" "https"'
### NOT WRITE
	#git_cmd='echo "$(git -C ${git_dir} remote -v)"'
	#git_cmd='echo "$(git -C ${git_dir} remote show origin)"'

	#git_cmd='echo -e "==> \\e[32m$(getGitRefURL ${git_dir} origin)\\e[0m"'
	#git_cmd='echo -en "==> \\e[32m$(getGitRefURL ${git_dir} origin)\\e[0m "; [ $(checkGitRef "${git_dir}" "origin") == 0 ] && echo -e "=> \\e[32mValid\\e[0m" || echo -e "=x \\e[31mBreak\\e[0m"'

	#git_cmd='echo -e "==> \\e[34m$(realpath --relative-to=. ${git_dir})\\e[0m"'
	#git_cmd='echo -e "==> \\e[34m$(realpath --relative-to=. ${git_dir})\\e[0m \\e[32m$(getGitRefURL ${git_dir} origin)\\e[0m"'

	#git_cmd='gdt_file_path="git_dtree.gdt"; echo "$(realpath --relative-to=. ${git_dir}) $(getGitRefURL ${git_dir} origin)" | tee -a "${gdt_file_path}"'	#rm "./git_dtree.gdt"
#[END SETTINGS SECTION]

git_dirs=()
#---------------------------------------------------------------
LOG_Info() {
	local _msg="${1}"
	[ "${_msg}" = "" ] && _msg="UNDEFINED."
	echo -e "\e[34mLOG\e[0m: ${_msg}" >&2
}
LOG_Error() {
	local _code="${1}" # IF > 0 -> EXIT
	local _msg="${2}"

	[ "${_code}" = "" ] && _code="1"
	[ "${_msg}" = "" ] && _msg="UNDEFINED."

	echo -e "\e[31mERROR\e[0m (${_code}): ${_msg}" >&2
	[ "${_code}" -gt 0 ] && exit "${_code}"
}
#---------------------------------------------------------------
checkGitRefURL() {
	local git_url="${1}"
	local git_check_timeout="5"

	timeout "${git_check_timeout}" git ls-remote "${git_url}" &> /dev/null
	[ $? == 0 ] && echo 0 && return 0
	echo 1 && return 1
}
getGitRefURL() {
	local git_dir="${1}"
	local git_ref="${2}"

	local git_url_act=""
	git_url_act="$(git -C ${git_dir} remote get-url ${git_ref})"
	echo "${git_url_act}"
}
setGitRefURL() {
	local git_dir="${1}"
	local git_ref="${2}"
	local git_url="${3}"

	local git_url_act=""
	git -C "${git_dir}" remote set-url "${git_ref}" "${git_url}"
	return $?
}

checkGitRef() {
	local git_dir="${1}"
	local git_ref="${2}"

	local git_url_act="$(getGitRefURL ${git_dir} ${git_ref})"
	[ $(checkGitRefURL "${git_url_act}") == 0 ] && echo 0 && return 0
	echo 1 && return 1
}

setGitProto() { # $?{ 0="changed"; 1="nothing to do"; 2="change error" }
	local git_dir="${1}"
	local git_ref="${2}"
	local git_proto_new="${3}"
	#local git_protocols=("git" "http" "https" "ssh")

	local git_url_act="$(getGitRefURL ${git_dir} ${git_ref})"
	local git_proto_act="${git_url_act%://*}"
	local git_url_new="${git_url_act/${git_proto_act}/}"

	[ "${git_proto_act}" == "${git_proto_new}" ] && echo -e "act: ${git_url_act}" >&2 && return 1

	git_url_new="${git_proto_new}${git_url_new}"
	echo -e "act: ${git_url_act}\nnew: ${git_url_new}" >& 2

	checkGitRefURL "${git_url_new}" &> /dev/null
	if [ $? == 0 ]; then
		echo "CHANGE"
		setGitRefURL "${git_dir}" "${git_ref}" "${git_url_new}" &> /dev/null
		return 0
	fi
	return 2
}
#---------------------------------------------------------------
getGitDirs() {
	if [ -d "${1}" ]; then
		for git_dir in ${1}* ; do
			if [ -d "${git_dir}" ]; then
				git_dir="$(realpath ${git_dir})"
				if [ "$(git -C ${git_dir} rev-parse --git-dir 2>/dev/null)" == ".git" ]; then
					git_dirs+=("${git_dir}")
				else
					getGitDirs "${git_dir}/" #RECURSION
				fi
			fi
		done
	fi
}
#---------------------------------------------------------------
main() {
	[ "$(command -v git)" == "" ] && LOG_Error 1 "git program not found."
	[ "${git_cmd}" == "" ] && LOG_Error 1 "git_cmd is empty."

	getGitDirs "${base_dir}"

	[ -z "$git_dirs" ] && LOG_Error 1 "none git directory found (${base_dir})."

	LOG_Info "==> base_dir: \e[33m${base_dir}\e[0m"
	LOG_Info "==> git_dirs list:"
	printf '===> %s\n' "${git_dirs[@]}"
	LOG_Info "==> dir_num: \e[33m${#git_dirs[@]}\e[0m"
	LOG_Info "==> git_cmd: \e[33m${git_cmd}\e[0m"

	while true; do
		read -p "Exec git_cmd on all git directories? [y/n] " -r -n 1 yn
		echo
		case "$yn" in
			[Yy]*)
				echo -e "\e[32mGIT EXEC:\e[0m"
				for git_dir in "${git_dirs[@]}" ; do
					LOG_Info "=> \e[33m${git_dir}\e[0m"
					eval "${git_cmd}"
				done
				break;;
			[Nn]*) break;;
			*) echo "Please type [Yy] or [Nn]";;
		esac
	done

	while true; do
		read -p "Press enter to exit " -s -r -n 1 enter
		echo
		[ "$enter" == "" ] && break
	done
}
#---------------------------------------------------------------
trap 'exit' 3

main
exit 0

