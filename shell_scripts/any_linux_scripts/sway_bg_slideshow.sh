#!/bin/bash

## Author: RorraVox
## File name: swaybar_bg_slideshow.sh
## LAST TESTED SWAY VERSION: 1.0-rc1

## USAGE EXAMPLE (append in sway config):
# exec ~/.config/sway/sway_bg_slideshow.sh ~/.config/sway/bg 120 &> /dev/null

# INPUT OPTIONS
#$1 = img_dir_path (without final slash EX:/home/user/images)
#$2 = int_time_sec

#[START SETTINGS SECTION]
int_time_sec="180" # MAX 7200 (2h)
img_dir_path=""
image_ext="png"
#[END SETTINGS SECTION]

# READ INPUT OPTIONS
if [ $# -ge 0 ]; then
	if [ "$1" != "" ]; then
		if [ -d "$1" ]; then
			img_dir_path="$1"
		else
			echo "ERROR: [$1] is not a directory!"
			exit 1
		fi
		if [ "$2" != "" ]; then
			if [ "$2" -ge 0 ] && [ "$2" -le 7200 ]; then # MAX 7200 (2h)
				int_time_sec="$2"
			fi
		fi
	fi
fi

#[START GLOBAL VARS]
declare -a bg_modes=("fill" "strech" "fit" "center" "tile") #DEFAULT: fill
#declare -a img_slide_modes=("random-once" "random-repeat" "read-order") #DEFAULT: random-once
declare -a img_paths
#[END GLOBAL VARS]

read_img_paths() {
	if [ -d "${img_dir_path}" ]; then
		for img_path in $img_dir_path/*.$image_ext; do
			if [ -f "${img_path}" ]; then
				img_paths[${#img_paths[@]}]="${img_path}"
			fi
		done
	fi
}
print_img_paths() {
	if [ "${#img_paths[@]}" -eq "0" ]; then
		echo "No image found! (${img_dir_path})"
		return
	else
		echo "Images (${img_dir_path})[${#img_paths[@]}]:"
		for img_path in "${img_paths[@]}"; do
			echo $img_path
		done
	fi
}
sway_bg_slideshow() {
	if [ "${#img_paths[@]}" -eq "0" ]; then
		echo "ERROR: no image found! (${img_dir_path})"
		return
	fi
	
	counter=$(( RANDOM % "${#img_paths[@]}" ))
	if ! [ -z "${SWAYSOCK}" ]; then
		while ! [ -z "${SWAYSOCK}" ]; do
			counter=$(( counter + 1))
			if [ "${counter}" -ge "${#img_paths[@]}" ]; then counter=0; fi
			image_id=$((counter % "${#img_paths[@]}"))
			img_path="${img_paths[$image_id]}"
			echo "Image: ${img_path}"
			swaymsg "output ${output} bg ${img_path} ${bg_mode}"
			sleep $int_time_sec
		done
	else
		echo "ERROR: no sway instance found!"
	fi
	
}

main() {
	echo "Sway socket: ${SWAYSOCK}"
	echo "Slide time: ${int_time_sec}"

	read_img_paths

	print_img_paths
	echo

	output="*"
	bg_mode="${bg_modes[0]}"

	sway_bg_slideshow
}

trap 'exit' 3
main
exit 0
