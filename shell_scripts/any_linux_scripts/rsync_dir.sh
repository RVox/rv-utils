#!/bin/bash

## Author: RorraVox
## File name: rsync_dir.sh

### INTERNAL ASSIGNMENT
#rsync_src=""
#rsync_dst=""
#rsync_opt=""

### EXTERNAL ASSIGNMENT
# eval rsync_src="<rsync_src>" rsync_dst="<rsync_dst>" rsync_opt="<rsync_opt>\ <rsync_opt>\ <rsync_opt>" "<rsync_dir_script_path>"

### --recursive --links --safe-links --hard-links --perms --executability --times --size-only --delete-after --itemize-changes --progress --verbose
# --recursive		| -r	recurse into directories
# --links			| -l	copy symlinks as symlinks
# --safe-links		| --	ignore symlinks that point outside the tree
# --copy-links		| -L	transform symlink into referent file/dir
# --hard-links		| -H	preserve hard links
# --perms			| -p	preserve permissions
# --executability	| -E	preserve executability
# --times			| -t	preserve modification times
# --atimes			| -U	preserve access (use) times
# --crtimes			| -N	preserve create times (newness)		[	This rsync does not support --crtimes (-N)	]
# --size-only		| --	skip files that match in size
# --delete-after	| --	receiver deletes after transfer, not during
# --itemize-changes	| -i	output a change-summary for all updates
# --progress		| --	show progress during transfer
# --verbose			| -v	increase verbosity
# --dry-run			| -n	perform a trial run with no changes made

#---------------------------------------------------------------
LOG_Info() {
    local _msg="${1}"
	[ "${_msg}" = "" ] && _msg="UNDEFINED."
	echo -e "\e[34mLOG\e[0m: ${_msg}" >&2
}
LOG_Error() {
	local _code="${1}" # IF > 0 -> EXIT
	local _msg="${2}"

	[ "${_code}" = "" ] && _code="1"
	[ "${_msg}" = "" ] && _msg="UNDEFINED."

    echo -e "\e[31mERROR\e[0m (${_code}): ${_msg}" >&2
	[ "${_code}" -gt 0 ] && exit "${_code}"
}
#---------------------------------------------------------------
main() {
	local default_rsync_opt="--recursive --links --safe-links --hard-links --perms --executability --times --size-only --delete-after --itemize-changes --progress --verbose"
	
	[ "${rsync_src}" == "${rsync_dst}" ] && LOG_Error 1 "rsync_src and rsync_src cannot be the same!"
	[ "${rsync_src}" == "" ] && LOG_Error 1 "rsync_src is empty."
	[ "${rsync_dst}" == "" ] && LOG_Error 1 "rsync_src is empty."
	[ "${rsync_opt}" == "" ] && rsync_opt="${default_rsync_opt}"

	LOG_Info "==> rsync_opt: ${rsync_opt}"
	LOG_Info "==> rsync_src: ${rsync_src}"
	LOG_Info "==> rsync_dst: ${rsync_dst}"
	LOG_Info "==> rsync_command: rsync ${rsync_opt} ${rsync_src} ${rsync_dst}"
	echo
	LOG_Info "Changes:"

	rsync ${rsync_opt} '--dry-run' "${rsync_src}" "${rsync_dst}" 

	while true; do
		read -p "Start rsync process? [y/n] " -r -n 1 yn
		echo
		case "$yn" in
			[Yy]*) break ;;
			[Nn]*) LOG_Error 1 "Aborted." ;;
			*) echo "Please type [Yy] or [Nn]" ;;
		esac
	done

	rsync ${rsync_opt} "${rsync_src}" "${rsync_dst}"
	LOG_Info "Sync finished."

	while true; do
		read -p "Press enter to exit " -s -r -n 1 enter
		echo
		[ "$enter" == "" ] && break
	done
}
#---------------------------------------------------------------

trap 'exit' 3

main
exit 0
