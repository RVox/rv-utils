#!/bin/bash

## Author: RorraVox
## File name: ps_swaybar.sh

### PrintStatus sway-bar/i3bar Config

## COMMANDS
# (echo, printf, awk, grep, cut, tr, wc, date, cat, head, tail, bc, df, ip, amixer, bluetoothctl, sensors)
## FONTS
# gohufont FOR TEXT
# siji	FOR SYMBOLS

#[START SETTINGS SECTION]
## Refresh Time
ref_time=1
#[END SETTINGS SECTION]

###############################################################
#FONT Siji (625 glyphs)
#	[NAME]	[UNI]	[SYM NAME]
# ps_anim_pacman
#	E0DE	E936	Pacman Silhouette
#	E0C6	E91E	Ghost Full
#	E140	EA4F	Ghost Silhouette
#	E14D	E9B7	Pacman Full
# ps_user_host
#	E1A3	E83C	Host User
# ps_temp
#	E01D	E95C	Thermometer
# ps_date
#	E225	E8A6	Calendar
# ps_time
#	E017	E9C8	Clock Night
#	E018	E9CF	Clock Day
# ps_cpu
#	E0C1	E919	CPU
# ps_mem
#	E0C5	E91D	Memory Available
#	E028	E9DF	Memory Used
#	E021	E9D8	Memory Total
# ps_power
#	E034	E9E5	Power Empty
#	E037	E9E8	Power Full
#	E036	E9E7	Power Charging
#	E035	E9E6	Power Discharging
# ps_audio
#	E052	E9F7	Audio Master Mute
#	E051	E9F6	Audio Master Play Low
#	E053	E9F8	Audio Master Play High
# ps_screen_bright
#	E1C2	E85B	Screen Bright High
#	E1C3	E85C	Screen Bright Medium
#	E1BC	E855	Screen Bright Low
# ps_network
#	E19C	E835	Network Wired
#	XXXX	XXXX	Network Radio
#	E0D0	E928	Network Upload Inactive
#	E0D1	E929	Network Download Inactive
#	E060	E9FF	Network Upload Active
#	E061	EA00	Network Download Active
# ps_bluetooth
#	E0B0	E908	Bluetooth Off
#	E00B	E8F3	Bluetooth On
#	E000	E8F1	Bluetooth On Disconnected
#	E02D	E963	Bluetooth On Connected
# ps_disc
#	E1D6	E8D8	Disc
#	E0AF	E907	Root
# ps_bar
#	E1B1	E84A	Separator
###############################################################

### ANIMATIONS
ps_anim_pacman() {
	declare -r -a anim_array=(
		"\uE936\uE91E"
		"\uEA4F\uE9B7")

	local time_mod=$(( 10#$(date "+%S") % 10#${#anim_array[@]} ))
	local out="${anim_array[${time_mod}]}"

	echo -e "${out}"
}
ps_anim_robotface() {
	declare -r -a anim_array=(
		"<|°_*|>"
		"<|*_°|>")

	local time_mod=$(( 10#$(date "+%S") % 10#${#anim_array[@]} ))
	local out="${anim_array[${time_mod}]}"

	echo -e "${out}"
}
### STATISTICS
ps_user_host() {
	local host_sym="\uE83C"
	echo -e "${host_sym}$USER@$(hostname)"
}
ps_temp() {
	local temp_sym="\uE95C"
	local temp=$(sensors | grep "Package id 0:" | awk 'NR==1{print $4"°C"}' FS=' +' RS='°C ')

	echo -e "${temp_sym}${temp}"
}
ps_date() {
	date_sym="\uE8A6"
	date=$(date "+%a %d/%m/%Y")
	echo -e "${date_sym}${date}"
}
ps_time() {
	local clock_day_sym="\uE9C8"
	local clock_night_sym="\uE9C8"
	local clock_sym=$clock_day_sym
	local day_start_h=7
	local day_end_h=20
	local time_hms=$(date "+%H:%M:%S")
	local time_h=$(date "+%H")

	if [ $time_h -ge $day_start_h ] && [ $time_h -le $day_end_h ]; then clock_sym=$clock_day_sym
	else clock_sym=$clock_night_sym
	fi
	echo -e "${clock_sym}${time_hms}"
}
ps_cpu() {
	local cpu_sym="\uE919"
	local last_cpu_times_path="/tmp/ps_swaybar_last_cpu_times.tmp"
	#proc_cpu_load_avg=$(cat /proc/loadavg | awk '{print $1}')
	local proc_cpu_times=$(cat /proc/stat | awk 'NR==1')
	local last_cpu_idle_time=0
	local last_cpu_load_time=0
	local cpu_idle_time=0
	local cpu_load_time=0
	local cpu_total_time=0
	local cpu_idle_time_d=0
	local cpu_total_time_d=0
	local cpu_per=0

	if [ -f "${last_cpu_times_path}" ]; then
		last_cpu_idle_time=$(cat "${last_cpu_times_path}" | awk 'NR==1')
		last_cpu_load_time=$(cat "${last_cpu_times_path}" | awk 'NR==2')
	fi
	cpu_idle_time=$(echo $proc_cpu_times | awk '{print($5 + $6)}')
	cpu_load_time=$(echo $proc_cpu_times | awk '{print($2 + $3 + $4 + $7 + $8 + $9)}')
	echo "${cpu_idle_time}" > "${last_cpu_times_path}"
	echo "${cpu_load_time}" >> "${last_cpu_times_path}"
	cpu_total_time=$(( $cpu_idle_time + $cpu_load_time ))
	last_cpu_total_time=$(( $last_cpu_idle_time + $last_cpu_load_time ))
	cpu_idle_time_d=$(( $cpu_idle_time - $last_cpu_idle_time ))
	cpu_total_time_d=$(( $cpu_total_time - $last_cpu_total_time ))
	cpu_per=$(bc <<< "scale=2; (${cpu_total_time_d} - ${cpu_idle_time_d}) / ${cpu_total_time_d} * 100" | awk '{print int($1)}')

	echo -e "${cpu_sym}$(printf "%03d" $cpu_per)%"
}
ps_mem() {
	local mem_avail_sym="\uE91D"
	local mem_used_sym="\uE9DF"
	local mem_total_sym="\uE9D8"
	local mem_unit="MiB"
	local mem_avail=$(cat /proc/meminfo | awk '/MemFree:/ {print $2}')
	local mem_total=$(cat /proc/meminfo | awk '/MemTotal:/ {print $2}')
	local mem_buff_cache=$(( $(cat /proc/meminfo | awk '/Buffers:/ {print $2}') + $(cat /proc/meminfo | grep ^Cached: | awk '{print $2}') ))
	local mem_avail=$(( $mem_avail + $mem_buff_cache ))
	local mem_used=$(( $mem_total - $mem_avail ))
	local mem_avail=$(( $mem_avail / 1024 ))
	local mem_used=$(( $mem_used / 1024 ))
	local mem_total=$(( $mem_total / 1024 ))
	local mem_used_per=$(bc <<< "scale=2; ((${mem_used} / ${mem_total}) * 100)" | awk '{print int($1)}')
	local mem_avail_per=$(bc <<< "scale=2; ((${mem_avail} / ${mem_total}) * 100)" | awk '{print int($1)}')

	echo -e "${mem_avail_sym}$(printf "%04d" $mem_avail)${mem_unit}[$(printf "%03d" $mem_avail_per)%]${mem_used_sym}$(printf "%04d" $mem_used)${mem_unit}[$(printf "%03d" $mem_used_per)%]${mem_total_sym}${mem_total}${mem_unit}"
}
ps_power() {
	#BATTERY
	local batt_sym_empty="\uE9E5"
	local batt_sym_full="\uE9E8"
	local batt_sym_char="\uE9E7"
	local batt_sym_unch="\uE9E6"
	local batt_sym=$batt_sym_empty
	local batt_value_charg_status=$(cat /sys/class/power_supply/BAT1/status | grep "Charging" | wc -l)
	local batt_value_capacity=$(cat /sys/class/power_supply/BAT1/capacity)

	if [ $batt_value_charg_status -eq 1 ]; then
		time_mod=$(( 10#$(date "+%S") % 4 ))
		case $time_mod in
			[0]) batt_sym=$batt_sym_empty;;
			[1]) batt_sym=$batt_sym_unch;;
			[2]) batt_sym=$batt_sym_char;;
			[3]) batt_sym=$batt_sym_full;;
		esac
	else
		if [ $batt_value_capacity -le 25 ]; then batt_sym=$batt_sym_empty
		elif [ $batt_value_capacity -le 50 ]; then batt_sym=$batt_sym_unch
		elif [ $batt_value_capacity -le 75 ]; then batt_sym=$batt_sym_char
		else batt_sym=$batt_sym_full
		fi
	fi
	echo -e "${batt_sym}$(printf "%03d" $batt_value_capacity)%"
}
ps_audio() {
	local audio_master_mute_sym="\uE9F7"
	local audio_master_play_l_sym="\uE9F6"
	local audio_master_play_h_sym="\uE9F8"
	local audio_sym=$audio_master_play_l_sym
	local audio_master_right_per=$(amixer sget Master | grep 'Front Right:' | awk -F'[][%]' '{ print $2 }')
	local audio_master_left_per=$(amixer sget Master | grep 'Front Left:' | awk -F'[][%]' '{ print $2 }')
	local audio_master_mute_status=$(amixer sget Master | awk -F'[][]' '{ print $4 }' | grep -m1 "on" | wc -l)

	if [ $audio_master_mute_status -eq 0 ]; then audio_sym=$audio_master_mute_sym
	else
		if [ $audio_master_right_per -ge 50 ] && [ $audio_master_left_per -ge 50 ]; then audio_sym=$audio_master_play_h_sym
		else audio_sym=$audio_master_play_l_sym
		fi
	fi
	echo -e "${audio_sym}[R$(printf "%03d" $audio_master_right_per)%$(printf "%03d" $audio_master_left_per)L]"
}
ps_screen_bright() {
	local screen_bright_h_sym="\uE85B"
	local screen_bright_m_sym="\uE85C"
	local screen_bright_l_sym="\uE855"
	local screen_bright_sym=$screen_bright_m_sym
	local screen_target_path="/sys/class/backlight/intel_backlight"
	local screen_max_bright=$(cat $screen_target_path/max_brightness)
	local screen_actual_bright=$(cat $screen_target_path/actual_brightness)

	local screen_actual_bright_per=$(echo $(bc <<< "scale=2; (${screen_actual_bright} / ${screen_max_bright}) * 100") | awk '{print int($1)}')
	if [ $screen_actual_bright_per -le 33 ]; then screen_bright_sym=$screen_bright_l_sym
	elif [ $screen_actual_bright_per -ge 66 ]; then screen_bright_sym=$screen_bright_h_sym
	else screen_bright_sym=$screen_bright_m_sym
	fi
	echo -e "${screen_bright_sym}$(printf "%03d" $screen_actual_bright_per)%"
}
## TESTED FOR ONE CONNECTION
ps_network() {
	local wire_net_sym="\uE835"
	local wifi_net_sym=""
	local up_inactive_net_sym="\uE928"
	local down_inactive_net_sym="\uE929"
	local up_active_net_sym="\uE9FF"
	local down_active_net_sym="\uEA00"
	local up_net_sym=$up_inactive_net_sym
	local down_net_sym=$down_inactive_net_sym
	local last_rx_tx_path="/tmp/ps_swaybar_${interface}_last_rx_tx.tmp"

	if [ "$(ip a | grep -e global)" != "" ]; then
		interface=$(ip route get 8.8.8.8 | tr -s ' ' | cut -d' ' -f5)
		ip_addr=$(ip route get 8.8.8.8 | tr -s ' ' | cut -d' ' -f7)
		out="${interface}: ${ip_addr}"
		last_rx=0
		last_tx=0
		if [ -f "${last_rx_tx_path}" ]; then
			last_rx=$(cat "${last_rx_tx_path}" | awk 'NR==1')
			last_tx=$(cat "${last_rx_tx_path}" | awk 'NR==2')
		fi
		rx=$(ip -s link show $interface | awk 'NR==4{print($1)}')
		tx=$(ip -s link show $interface | awk 'NR==6{print($1)}')
		echo "${rx}" > "${last_rx_tx_path}"
		echo "${tx}" >> "${last_rx_tx_path}"
		up=$(( tx - last_tx ))
		down=$(( rx - last_rx ))
		if [ $up -gt 0 ]; then up_net_sym=$up_active_net_sym
		else up_net_sym=$up_inactive_net_sym
		fi
		if [ $down -gt 0 ]; then down_net_sym=$down_active_net_sym
		else down_net_sym=$down_inactive_net_sym
		fi
		out="${out}[${up_net_sym}($(printf "%06d" $up)):${down_net_sym}($(printf "%06d" $down))]"
	else
		out="!NO CONN FOUND!"
	fi
	echo -e "${wire_net_sym}${out}"
}
ps_bluetooth() {
	local bt_sym_off="\uE908"
	local bt_sym_on="\uE8F3"
	local bt_sym_dev_disc="\uE8F1"
	local bt_sym_dev_conn="\uE963"
	local bt_sym=$bt_sym_off
	local bt_sym_dev=$bt_sym_dev_disc
	local bt_status=0

	if [ "$(bluetoothctl show | grep "Powered: yes")" == "" ]; then bt_status=0
	else bt_status=1
	fi
	if [ $bt_status -eq 1 ]; then bt_sym=$bt_sym_on
		if ! [ "$(bluetoothctl info | grep "Device")" == "" ]; then bt_sym_dev=$bt_sym_dev_conn
		else bt_sym_dev=$bt_sym_dev_disc
		fi
	else
		bt_sym=$bt_sym_off
	fi
	echo -e "${bt_sym}${bt_sym_dev}"
}
ps_disc() {
	local disc_sym="\uE8D8"
	local root_sym="\uE907"
	local mem_unit="GiB"

	disc_root=$(df -BGiB | grep "/$")
	disc_dev_root=$(echo $disc_root | awk '{print $1}')
	if [ $(echo $disc_dev_root | grep / ) != "" ]; then disc_dev_root=$(echo $disc_dev_root | cut -f3 -d"/"); fi #TO FREE MORE SPACE AS POSSIBLE (REMOVE /dev/)
	disc_space_total_root=$(echo $disc_root | awk '{print $2}' | head -c -4)
	disc_space_used_root=$(echo $disc_root | awk '{print $3}' | head -c -4)
	disc_space_free_root=$(echo $disc_root | awk '{print $4}' | head -c -4)
	echo -e "${disc_sym}${disc_dev_root}:$(printf "%03d" $disc_space_free_root)${mem_unit}|$(printf "%03d" $disc_space_used_root)${mem_unit}/$(printf "%03d" $disc_space_total_root)${mem_unit}"
}

### MAIN FUNCTIONS
ps_bar_notebook() {
	local sep_sym="\uE84A"

	local ps_out=""
	ps_out+="$(ps_anim_pacman)${sep_sym}"
	ps_out+="$(ps_user_host)${sep_sym}"
	ps_out+="$(ps_disc)${sep_sym}"
	ps_out+="$(ps_cpu)${sep_sym}"
	ps_out+="$(ps_mem)${sep_sym}"
	ps_out+="$(ps_network)${sep_sym}"
	ps_out+="$(ps_bluetooth)${sep_sym}"
	ps_out+="$(ps_audio)${sep_sym}"
	ps_out+="$(ps_temp)${sep_sym}"
	ps_out+="$(ps_power)${sep_sym}"
	ps_out+="$(ps_screen_bright)${sep_sym}"
	ps_out+="$(ps_date)$(ps_time)"

	echo -e "${ps_out}"
}
ps_bar_dektop() {
	local sep_sym="\uE84A"

	local ps_out=""
	ps_out+="$(ps_anim_pacman)${sep_sym}"
	ps_out+="$(ps_user_host)${sep_sym}"
	ps_out+="$(ps_disc)${sep_sym}"
	ps_out+="$(ps_cpu)${sep_sym}"
	ps_out+="$(ps_mem)${sep_sym}"
	ps_out+="$(ps_network)${sep_sym}"
		#ps_out+="$(ps_bluetooth)${sep_sym}" #ERROR
	ps_out+="$(ps_audio)${sep_sym}"
	ps_out+="$(ps_temp)${sep_sym}"
		#ps_out+="$(ps_power)${sep_sym}"
		#ps_out+="$(ps_screen_bright)${sep_sym}"
	ps_out+="$(ps_date)$(ps_time)"

	echo -e "${ps_out}"
}

ps_bar() {
	ps_bar_dektop
	#ps_bar_notebook
}

main() {
	while :;do
		ps_bar
		sleep ${ref_time}
	done
}

trap 'exit' 3
main
exit 0
