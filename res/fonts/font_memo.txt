# install custom system fonts on /usr/share/fonts/
# install custom user fonts on ~/.local/share/fonts/

Font formats
	Bitmap
		.bdf	Glyph Bitmap Distribution Format (BDF)
		.pcf	Portable Compiled Format (PCF) 
	Vector
		.ttf	TrueType Fonts (TTF)
		.otf	OpenType Fonts (OTF)


# List all installed fonts
#C	fc-list
