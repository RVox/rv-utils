# [rv-utils]

```
   ▒▒▒▒▒▒▒▒▒   
  ▒▒       ▒▒  
 ▒▒         ▒▒ 
▒▒           ▒▒
▒▒ ▼▼▼▼ ▼▼▼▼ ▒▒
▓▒ ▼▼▼   ▼▼▼ ▒▓
 ▒▓▒  ▲▲▲  ▒▓▒ 
  ▒ ▒     ▒ ▒  
  ▒▓▓     ▓▓▒  
  ▓▓▓     ▓▓▓  
   ▓▓▓ ▓ ▓▓▓   
    ▓▓▓▓▓▓▓    
```

## Code Overview
- [`dot_files`](dot_files/)
- [`res`](res/)
- [`shell_scripts`](shell_scripts/)



## Notation
Executable command
```#C [something]```

Custom user variables
```#[START SETTINGS SECTION]-#[END SETTINGS SECTION]``` 

Default heading
``` 
## Author: RorraVox
## File name: <file_name>
## File path: <file_path>

### Description
```

